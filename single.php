<?php  get_header();
require_once('components/navbar.inc.php');
if ( have_posts() ) {
while ( have_posts() ) {
the_post();
?>

<!--Main layout-->
<main>
    <div class="container">

        <!--Section: Post-->
        <section class="mt-3">

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-9 mb-4">

                    <!-- Breadcrumbs -->
                    <?php
                    $categories = get_the_category();
                    $first_category_name = $categories[0]->cat_name;
                    $first_category_id = get_cat_ID( $category[0]->cat_name );
                    $first_category_link = get_category_link( $category_id );
                    ?>
                    <ol class="breadcrumb white z-depth-1">
                        <li class="breadcrumb-item">
                            <a href="<?php echo get_home_url(); ?>">Home</a>
                        </li>
                        <?php
                        if (count($categories)){
                        ?>
                        <li class="breadcrumb-item">
                            <a href="<?php echo $first_category_link ?>"><?php echo $first_category_name ?></a>
                        </li>
                        <?php
                        }
                        ?>
                        <li class="breadcrumb-item active"><?php the_title() ?></li>
                    </ol>
                    <!-- Breadcrumbs -->

                    <!-- Featured image -->
                    <div style="width: 60%;">
                        
                    <?php the_post_thumbnail( 'medium-large', array( 'class'=> 'img-fluid  mb-4'));?>
                    </div>
                   <!--Card-->
                    <div class="card mb-4">

                        <!--Card content-->
                        <div class="card-body">

                            <p>Posted on <?php echo get_the_date(); ?></p>

                            <hr>

                            <div class="post-content">
                            <?php the_content(); ?>
                            </div>

                        </div>

                    </div>
                    <!--/.Card-->

                    <!--Comments and reply-->
                    <?php comments_template(); ?>
                    <!--/.Comments and reply-->

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-3 mb-4">
                <!--Section: Dynamic Content Wrapper-->                 
                   <!--Sidebar-->
                        <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
                        <?php dynamic_sidebar( 'sidebar-1' ); ?>
                        <?php endif; ?>
                    <!--/.Sidebar-->
                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

        </section>
        <!--Section: Post-->

    </div>
</main>
<!--Main layout-->

<?php
} // end while
} // end if
get_footer();
?>