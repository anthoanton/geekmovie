

<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark scrolling-navbar">
    <div class="container">
        <!-- Brand -->
        <?php echo get_custom_logo(); ?>
        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-geek" aria-controls="menu-geek"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <?php wp_nav_menu(array('container_id'=> 'menu-geek')); ?>
    </div>
</nav>
<!-- Navbar -->