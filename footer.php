
    <!--Footer-->
    <footer class="page-footer font-small mt-4 wow fadeIn ">
    <div class="container">
    <div class="row wow fadeIn">
    <!--Grid column-->
    	<!--Sidebar-->
    	 <div class="col-lg-4 col-md-12 mb-4">
         	<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
            <?php dynamic_sidebar( 'sidebar-2' ); ?>
            <?php endif; ?>
        </div>
        <div class="col-lg-4 col-md-12 mb-4">
            <?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
            <?php dynamic_sidebar( 'sidebar-3' ); ?>
            <?php endif; ?>
        </div>
        <div class="col-lg-4 col-md-12 mb-4">
            <?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
            <?php dynamic_sidebar( 'sidebar-4' ); ?>
            <?php endif; ?>
        </div>   
        <!--/.Sidebar-->
    </div>
        
    </div>

        <!--Copyright-->
        <div class="footer-copyright py-3 text-center">
            © 2019 Copyright: <?php bloginfo("name"); ?>
        </div>
        <!--/.Copyright-->
<?php wp_footer(); ?>
</body>
<script>
	$("#mdb-navigation > ul > li").addClass("page-item")
	$("#mdb-navigation > ul > li > a").addClass("page-link")
	$("#menu-geek").addClass("collapse navbar-collapse")
	$("#menu-geek > ul").addClass("navbar-nav nav-flex-icons")
	$("#menu-geek > ul > li").removeClass()
	$("#menu-geek > ul > li").addClass("nav-item")
	$("#menu-geek > ul > li > a").addClass("nav-link waves-effect")
	$(".custom-logo-link").addClass("navbar-brand pt-0 waves-effect")
	
</script>
</html>