<?php

/**
 * Include external files
 */
require_once('inc/pagination.inc.php');
require_once('inc/template-tags.inc.php');

/**
 * Include CSS files
 */
function theme_enqueue_scripts() {
        wp_enqueue_style( 'Font_Awesome', get_template_directory_uri() . '/Font_Awesome.css' );
        wp_enqueue_style( 'Bootstrap_css', get_template_directory_uri() . '/css/bootstrap.min.css' );
        wp_enqueue_style( 'MDB', get_template_directory_uri() . '/css/mdb.min.css' );
        wp_enqueue_style( 'Style', get_template_directory_uri() . '/style.css' );
        wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/js/jquery-3.4.1.min.js', array(), '3.4.1', true );
        wp_enqueue_script( 'Tether', get_template_directory_uri() . '/js/popper.min.js', array(), '1.0.0', true );
        wp_enqueue_script( 'Bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );
        wp_enqueue_script( 'MDB', get_template_directory_uri() . '/js/mdb.min.js', array(), '1.0.0', true );

        }
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

/**
 * Setup Theme
 */
function mdbtheme_setup() {
    // Add featured image support
    add_theme_support('post-thumbnails');
    // This theme supports a custom header image.
      $args = array(
      'width' => 40,
      'height' => 40,
      'flex-width' => true,
      'flex-height' => true,
      'header-text' => false);

      add_theme_support( 'custom-logo', $args );

      // This theme supports custom background color and image.
      $defaults = array(
      'default-color' => '', 
      'default-image' => '',
      'wp-head-callback'  => '_custom_background_cb',
      'admin-head-callback' => '',
      'admin-preview-callback' => '' );  
      add_theme_support( 'custom-background', $defaults );

      // Adds RSS feed links to <head> for posts and comments.  
      add_theme_support( 'automatic-feed-links' );
      // This theme supports the Title Tag feature.
      add_theme_support( 'title-tag' );
}
add_action('after_setup_theme', 'mdbtheme_setup');


/**
 * Register our sidebars and widgetized areas.
 */
function mdb_widgets_init() {

  register_sidebar( array(
        'name' => 'Menu Derecho post',
        'id' => 'sidebar-1',
        'before_widget' => '<div id="%1$s" class="card mb-4 %2$s bg-colorr">',
        'after_widget' => '</div>',
        'before_title' => ' <p class="card-header">',
        'after_title' => '</p>',
    ) );
  register_sidebar( array(
        'name' => 'Footer Izquierdo',
        'id' => 'sidebar-2',
        'before_widget' => '<div id="%1$s" class=" footer-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<p class="footer-headline">',
        'after_title' => '</p>',
    ) );
  register_sidebar( array(
        'name' => 'Footer Medio',
        'id' => 'sidebar-3',
        'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<p class="footer-headline">',
        'after_title' => '</p>',
    ) );
  register_sidebar( array(
        'name' => 'Footer Derecho',
        'id' => 'sidebar-4',
        'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<p class="footer-headline">',
        'after_title' => '</p>',
    ) );
   register_sidebar( array(
        'name' => 'index Menu sup',
        'id' => 'sidebar-5',
        'before_widget' => '<div id="%1$s" class="card mb-4 %2$s bg-colorr">',
        'after_widget' => '</div>',
        'before_title' => ' <p class="card-header">',
        'after_title' => '</p>',
    ) );

}
add_action( 'widgets_init', 'mdb_widgets_init' );

?>