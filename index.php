<?php  get_header(); 
require_once('components/navbar.inc.php'); ?>

    <!--Main layout-->
    <main style="margin-top: 70px;">

        <div class="container">

            <!--Section: Dynamic Content Wrapper-->
            <section>
              <div id="dynamic-content"></div>

            </section>
            <!--Section: Dynamic Content Wrapper-->

            <!--Section: Articles-->
            <section>
                <div class="row wow fadeIn" >
                  <div class="col-lg-12 col-md-12 mb-12 " style="padding: 10px;">
                    <?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
                    <?php dynamic_sidebar( 'sidebar-5' );?>
                    <?php endif; ?>
                  </div> 
                  <div class="col-lg-12 col-md-6 mb-12">

                <div class="row wow fadeIn" >
                    <?php
                    if ( have_posts() ) {
                    $counter = 1;
                    while ( have_posts() ) {
                    the_post();
                    ?>

                      <!--Grid column-->
                      <div class="col-lg-2 col-md-6 mb-2">

                        <!--Card-->
                        <div class="card">

                          <!--Card image-->
                          <div class="view overlay">
                            <?php the_post_thumbnail( 'medium-large', array( 'class'=> 'img-fluid')); ?>
                            <a href="<?php echo get_permalink() ?>">
                              <div class="mask rgba-white-slight waves-effect waves-light"></div>
                            </a>
                          </div>
                          <!--Card image-->

                          <!--Card content-->
                          <div class="card-body text-center">
                            <!--Category & Title-->
                            <a href="<?php echo get_permalink() ?>" class="grey-text">
                              <h6><?php the_title(); ?></h6>
                            </a>
                            <span class="badge badge-pill danger-color"><?php the_tags('',' '); ?></span>
                          </div>
                          <!--Card content-->

                        </div>
                        <!--Card-->

                      </div>
                      <!--Grid column-->
                      <?php

                       if ($counter % 24 == 0) { 
                      ?>

                </div>
            </div>
            </div>
                <!--Grid dynamic row-->
              <div class="row wow fadeIn">
                <?php
                }
                $counter++;                  
                } // end while
                } // end if
                ?>
              </div>
               
              <!--Grid row-->
                  <?php mdb_pagination(); ?>  
            </section>
            <!--Section: Articles-->

        </div>
    </main>
    <!--Main layout-->

<?php  get_footer(); ?>